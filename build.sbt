lazy val akkaHttpVersion = "10.2.1"
lazy val akkaVersion    = "2.6.10"
lazy val testcontainersScalaVersion = "0.38.4"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "de.diskordanz",
      scalaVersion    := "2.12.10"
    )),
    name := "recipes",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"                  % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"                % akkaVersion,

      // auth
      "com.softwaremill.akka-http-session" %% "core"      % "0.5.11",
      "com.softwaremill.akka-http-session" %% "jwt"       % "0.5.11",
      "com.github.t3hnar" %% "scala-bcrypt"               % "4.3.0",

      "ch.megard"         %% "akka-http-cors"             % "1.1.0",

      // logging
      "com.typesafe.scala-logging" %% "scala-logging"     % "3.9.2",
      "ch.qos.logback" % "logback-classic"                % "1.2.3",
      "net.logstash.logback" % "logstash-logback-encoder" % "4.11",

      // json
      "com.typesafe.play" %% "play-json"                  % "2.9.1",
      "de.heikoseeberger" %% "akka-http-play-json"        % "1.35.0",

      "org.reactivemongo" %% "reactivemongo"              % "1.0.0",

      "com.github.slugify" % "slugify"                    % "2.4",

      "com.typesafe.akka"   %% "akka-testkit"               % akkaVersion     % Test,
      "com.typesafe.akka"   %% "akka-http-testkit"          % akkaHttpVersion % Test,
      "org.scalatest"       %% "scalatest"                  % "3.2.0"         % Test,
      "org.scalamock"       %% "scalamock"                  % "5.0.0"         % Test,
      "com.dimafeng"        %% "testcontainers-scala-scalatest" % testcontainersScalaVersion % Test,
      "com.dimafeng"        %% "testcontainers-scala-mongodb"   % testcontainersScalaVersion % Test
)
  )

assemblyMergeStrategy in assembly := {
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

assemblyJarName in assembly := "recipes.jar"