import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from 'axios'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueI18n from 'vue-i18n'
import messages from './i18n/messages'


Vue.use(VueI18n)
Vue.use(axios)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

const i18n = new VueI18n({
  locale: 'de',
  messages,
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
