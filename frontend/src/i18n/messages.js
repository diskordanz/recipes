export default {
    de: {
        message: {
            recipe: {
                name: "Name",
                preparation: "Zubereitung",
                tags: "Tags",
                ingredients: "Zutaten",
                duration: "Dauer",
                servings: "Portionen",
                action: {
                    createNew: "Neues Rezept",
                    save: "Speichern",
                    delete: "Löschen",
                    edit: "Bearbeiten",
                },
                ingredient: "Zutat"
            },
            auth: {
                login: "Einloggen",
                logout: "Ausloggen",
                user: "User",
                password: "Passwort",
            },
            tag: {
                placeholder: "Tags hinzufügen...",
                add: "+",
                duplicateExists: "Tag existiert bereits"
            },
            validation: {
                empty: "{0} darf nicht leer sein"
            }
        }
    }
}
