import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store/index'

Vue.use(VueRouter)

export const loginRoute = "/login"

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: loginRoute,
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/edit/:slug',
    name: 'EditRecipe',
    component: () => import('../views/EditRecipe.vue'),
    props: true
  },
  {
    path: '/recipes/:slug',
    name: 'Recipe',
    component: () => import('../views/Recipe.vue'),
    props: true
  },
  {
    path: '/new',
    name: 'CreateRecipe',
    component: () => import('../views/CreateRecipe.vue'),
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, _from, next) => {
  if (to.name !== 'Login' && !store.getters.sessionActive) {
    next('/login')
  } else {
    next()
  }
})

export default router
