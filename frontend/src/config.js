
let baseUrl = "/api/"
const baseUrlFromEnv = process.env.VUE_APP_API_BASE_URL
if (baseUrlFromEnv && baseUrlFromEnv.length > 0) {
    baseUrl = baseUrlFromEnv
}

export const config = {
    apiBaseUrl: baseUrl,
    isProduction: process.env.NODE_ENV === 'production'
}