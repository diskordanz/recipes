import axios from "axios";
import { config } from "./config"
import store from "./store/index"

const requestOptions = { withCredentials: true }

export default {
    login: (username, password) => {
        return axios.post(`${config.apiBaseUrl}auth/login`, {
            userName: username,
            password: password
        })
    },
    logout: () => {
        return axios.post(`${config.apiBaseUrl}auth/logout`, {}, requestOptions)
    },
    getRecipes: () => {
        return axios.get(`${config.apiBaseUrl}recipes/`,
            requestOptions)
    },
    searchRecipes: (query) => {
        return axios.get(`${config.apiBaseUrl}recipes/search?q=${query}`,
            requestOptions)
    },
    getRecipe: (slug) => {
        return axios.get(`${config.apiBaseUrl}recipes/${slug}`,
            requestOptions)
    },
    createRecipe: (recipe) => {
        return axios.post(`${config.apiBaseUrl}recipes/`,
            recipe,
            requestOptions)
    },
    deleteRecipe: (slug) => {
        return axios.delete(`${config.apiBaseUrl}recipes/${slug}`,
            requestOptions)
    },
    updateRecipe: (slug, recipe) => {
        return axios.put(`${config.apiBaseUrl}recipes/${slug}`,
            recipe,
            requestOptions)
    },
    getSession: () => {
        return axios.get(`${config.apiBaseUrl}auth/session`,
            requestOptions)
    }
}

axios.interceptors.response.use(
    (response) => {
        return response
    },
    (error) => {
        if (error.response.status === 401) {
            store.commit("unsetSession")
        }
        return Promise.reject(error)
    })