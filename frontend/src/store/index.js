import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    query: "",
    session: null
  },
  mutations: {
    setQuery(state, query) {
      state.query = query
    },
    unsetQuery(state) {
      state.query = ""
    },
    setSession(state, session) {
      state.session = session
    },
    unsetSession(state) {
      state.session = null
    }
  },
  getters: {
    getQuery(state) {
      return state.query
    },
    sessionActive(state) {
      return state.session !== null
    }
  }
})
