# Recipes

Simple VueJS/Akka web application for recipe management for my own use -- and learning purposes :)

The application currently uses a MongoDB as persistence layer.

## Known issues

- Authentication is a bit wonky, and I am aware that using a cookie accessible via JavaScript is unsafe. As authentication mainly exists to keep this a private site and prevent having to put imprint and data protection information on the site, I currently accept this drawback.

## Importing from webcookbook
(webcookbook is a previous iteration of this project)

```
http localhost:8080/api/recipes/import Cookie:<session cookie from browser> < wcbdump2.json 
```

The import format is database entries from webcookbook, converted from JSON lines to a JSON array.
