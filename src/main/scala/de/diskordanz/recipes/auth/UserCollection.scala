package de.diskordanz.recipes.auth

import com.softwaremill.session.SessionUtil
import com.typesafe.scalalogging.LazyLogging
import de.diskordanz.recipes.config.Configuration
import de.diskordanz.recipes.db.AbstractCollection
import reactivemongo.api.DB
import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.indexes.Index.Aux

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait UserRepository {

  def findByName(name: String): Future[Option[User]]

  def update(user: User): Future[Unit]

  def insert(user: User): Future[Unit]
}

object UserRepository extends LazyLogging {

  def initialize(db: DB, configuration: Configuration): Future[UserRepository] = {
    val collection = new UserCollection(db)
    for {
      _ <- ensureAdminUser(collection, configuration)
      _ <- collection.ensureIndexes()
    } yield {
      logger.info("Initialized user repository")
      collection
    }
  }.recoverWith {
    case e =>
      logger.error("Failed initializing user repository", e)
      Future.failed(e)
  }

  private def ensureAdminUser(userRepository: UserRepository, config: Configuration): Future[Unit] = {
    val passwordOverride = config.adminPassword

    userRepository.findByName(User.AdminUserName).flatMap {
      case None =>
        val adminPassword = passwordOverride.getOrElse(SessionUtil.randomString(64))
        if (passwordOverride.isEmpty) {
          logger.info(s"Created user 'admin' with password $adminPassword")
        }
        userRepository.insert(User.withPassword(User.AdminUserName, adminPassword, User.Roles.Admin))
      case Some(adminUser) =>
        passwordOverride match {
          case Some(password) =>
            userRepository
              .update(adminUser.withNewPasswordNoValidate(password))
              .map { _ =>
                logger.warn("Updated password for existing user 'admin' due to override")
              }
          case _ => Future.successful()
        }
    }
  }

}

private class UserCollection(db: DB) extends AbstractCollection(UserCollection.Name, db) with UserRepository {

  override protected val indexes: Seq[Aux[BSONSerializationPack.type]] = Nil

  private val projection: Option[BSONDocument] = None

  override def findByName(name: String): Future[Option[User]] = {
    collection.find(byName(name), projection)
      .one[User]
  }

  private def byName(name: String) = BSONDocument("name" -> name)

  override def update(user: User): Future[Unit] = {
    collection.update(ordered = false)
      .one(BSONDocument("name" -> user.name), user, upsert = false)
      .map(_ => ())
  }

  override def insert(user: User): Future[Unit] = {
    collection.insert(ordered = false)
      .one(user)
      .map(_ => ())
  }
}

private object UserCollection {
  val Name = "users"
}
