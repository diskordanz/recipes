package de.diskordanz.recipes.auth
import com.github.t3hnar.bcrypt._
import com.typesafe.scalalogging.LazyLogging
import de.diskordanz.recipes.api.{AuthenticationFailure, ErrorMessage}
import play.api.libs.json.{Json, OWrites}
import reactivemongo.api.bson.{BSONDocumentReader, BSONDocumentWriter, Macros}

import scala.util.{Failure, Success}


case class User(name: String, private val passwordHash: String, role: String) extends LazyLogging {

  def isPasswordValid(password: String): Boolean = {
    password.isBcryptedSafe(passwordHash) match {
      case Success(result) => result
      case Failure(cause) =>
        logger.error("Authentication failed because of underlying exception", cause)
        false
    }
  }

  def withNewPassword(oldPassword: String, newPassword: String): Either[ErrorMessage, User] = {
    if (isPasswordValid(oldPassword)) {
      Right(copy(passwordHash = newPassword.bcrypt))
    } else {
      Left(AuthenticationFailure)
    }
  }

  def withNewPasswordNoValidate(newPassword: String): User = copy(passwordHash = newPassword.bcrypt)

  def toSessionInfo: SessionInfo = SessionInfo(name)

  override def toString: String = name
}

object User {

  val AdminUserName = "admin"

  def withPassword(name: String, password: String, role: String): User = new User(name, password.bcrypt, role)

  object Fields {
    val Name = "name"
    val Role = "role"
    val PasswordHash = "passwordHash"
  }

  implicit val writer: BSONDocumentWriter[User] = Macros.writer[User]
  implicit val reader: BSONDocumentReader[User] = Macros.reader[User]

  object Roles {
    val Admin = "admin"
    val User = "user"
  }

}

case class SessionInfo(userName: String)

object SessionInfo {
  implicit val jsonw: OWrites[SessionInfo] = Json.writes[SessionInfo]
}
