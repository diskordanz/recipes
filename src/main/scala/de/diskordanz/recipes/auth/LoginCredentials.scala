package de.diskordanz.recipes.auth

import play.api.libs.json.{Format, JsValue, Json}

case class LoginCredentials(userName: String, password: String) {

  def toJson: JsValue = Json.toJson(this)
}

object LoginCredentials {
  implicit val format: Format[LoginCredentials] = Json.format[LoginCredentials]
}