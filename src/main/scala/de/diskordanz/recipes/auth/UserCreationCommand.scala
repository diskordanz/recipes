package de.diskordanz.recipes.auth

import de.diskordanz.recipes.auth.User.Roles
import play.api.libs.json.{Json, Reads}

case class UserCreationCommand(user: String, password: String) {

  def toUser: User = User.withPassword(user, password, Roles.User)
}

object UserCreationCommand {
  implicit val jsonr: Reads[UserCreationCommand] = Json.reads[UserCreationCommand]
}
