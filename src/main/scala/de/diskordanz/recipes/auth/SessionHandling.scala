package de.diskordanz.recipes.auth

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.onComplete
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.stream.Materializer
import com.softwaremill.session.CsrfDirectives.setNewCsrfToken
import com.softwaremill.session.CsrfOptions.checkHeaderAndForm
import com.softwaremill.session.SessionDirectives._
import com.softwaremill.session.SessionOptions.{oneOff, usingCookiesOrHeaders, usingHeaders}
import com.softwaremill.session._
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JValue

import scala.util.Success

trait SessionHandling extends LazyLogging {

  val userRepository: UserRepository

  implicit val sessionManager: SessionManager[String] = {
    implicit val serializer: SessionSerializer[String, JValue] = JValueSessionSerializer.stringToJValueSessionSerializer
    implicit val encoder: JwtSessionEncoder[String] = new JwtSessionEncoder[String]()
    val sessionConfig = SessionConfig.fromConfig(
      ConfigFactory.load()
        .withValue("akka.http.session.server-secret", ConfigValueFactory.fromAnyRef(SessionUtil.randomServerSecret())))
    new SessionManager[String](sessionConfig)
  }

  def authenticated(d: User => Route)(implicit sessionManager: SessionManager[String]): Route = {
    optionallyAuthenticated {
      case Some(user) => d(user)
      case _ =>
        logger.warn("Unauthorized login attempt")
        complete(StatusCodes.Unauthorized)
    }
  }

  def optionallyAuthenticated(d: Option[User] => Route)(implicit sessionManager: SessionManager[String]): Route = {
    optionalSession(oneOff, usingCookiesOrHeaders) {
      case Some(session) =>
        val futureUser = userRepository.findByName(session)
        onComplete(futureUser) {
          case Success(maybeUser) => d(maybeUser)
          case _ => complete(StatusCodes.Unauthorized)
        }
      case None => complete(StatusCodes.Unauthorized)
    }
  }

  def tryLoggingInUser(loginCredentials: LoginCredentials)(implicit sessionManager: SessionManager[String], materializer: Materializer): Route = {
    val maybeUser = userRepository.findByName(loginCredentials.userName)
    onComplete(maybeUser) {
      case Success(Some(user)) =>
        if (user.isPasswordValid(loginCredentials.password)) {
          startUserSession(loginCredentials.userName)
        } else {
          logger.warn("Illegal login attempt")
          complete(StatusCodes.Unauthorized)
        }
      case _ => complete(StatusCodes.Unauthorized)
    }
  }

  private def startUserSession(userName: String)(implicit sessionManager: SessionManager[String], materializer: Materializer): Route = {
    logger.info(s"Logging in ${userName}")
    setSession(oneOff, usingHeaders, userName) {
      setNewCsrfToken(checkHeaderAndForm) {
        complete(StatusCodes.OK)
      }
    }
  }

  def logoutUser(user: User)(implicit sessionManager: SessionManager[String]): Route = {
    logger.info(s"Logging out user ${user.name}")
    invalidateSession(oneOff, usingCookiesOrHeaders) {
      complete(StatusCodes.OK)
    }
  }

}
