package de.diskordanz.recipes.db

import com.typesafe.scalalogging.LazyLogging
import reactivemongo.api.DB
import reactivemongo.api.bson.collection.{BSONCollection, BSONSerializationPack}
import reactivemongo.api.indexes.Index.Aux

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

abstract class AbstractCollection(name: String, db: DB)(implicit executionContext: ExecutionContext) extends LazyLogging {

  protected val indexes: Seq[Aux[BSONSerializationPack.type]]

  protected val collection: BSONCollection = db.collection(name)

  def ensureIndexes(): Future[Unit] = {
    val indexFutures = for {
      index <- indexes
    } yield
      collection.indexesManager.ensure(index).andThen {
        case Success(true) => logger.info("Created index {}", index.eventualName)
        case Success(false) => logger.info("Index {} already present", index.eventualName)
        case Failure(cause) => logger.warn("Failed creating index {}", index.eventualName, cause)
      }

    Future.sequence(indexFutures).map(_ => ())
  }
}
