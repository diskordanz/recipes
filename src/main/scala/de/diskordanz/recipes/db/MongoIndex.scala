package de.diskordanz.recipes.db

import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.indexes.{IndexType, Index => I}

object MongoIndex {

  type Index = I.Aux[BSONSerializationPack.type]

  def unique(fields: (String, IndexType)*): Index = I(BSONSerializationPack)(
    key = fields,
    unique = true, background = false, sparse = false,
    expireAfterSeconds = None, name = None, storageEngine = None, weights = None, defaultLanguage = None,
    languageOverride = None, textIndexVersion = None, sphereIndexVersion = None, bits = None, min = None, max = None,
    bucketSize = None, collation = None, wildcardProjection = None, version = None, partialFilter = None,
    options = BSONDocument.empty)

  def fullTextOnFields(language: String, fields: Seq[String]): Index = I(BSONSerializationPack)(
    key = fields.map((_, IndexType.Text)),
    unique = false, background = false, sparse = false,
    expireAfterSeconds = None, name = None, storageEngine = None, weights = None, defaultLanguage = Some(language),
    languageOverride = Some(language), textIndexVersion = None, sphereIndexVersion = None, bits = None, min = None, max = None,
    bucketSize = None, collation = None, wildcardProjection = None, version = None, partialFilter = None,
    options = BSONDocument.empty)
}
