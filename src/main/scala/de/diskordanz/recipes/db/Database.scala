package de.diskordanz.recipes.db

import com.typesafe.scalalogging.LazyLogging
import reactivemongo.api.{AsyncDriver, DB, MongoConnection}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Database extends LazyLogging {

  val Name = "db"

  def getConnection(mongoDbUri: String): Future[DB] = {

    val driver = AsyncDriver()
    val parsedUri = MongoConnection.fromString(mongoDbUri)
    parsedUri
      .flatMap(x => driver.connect(x))
      .flatMap(_.database(Name))
  }

}





