package de.diskordanz.recipes.api

import play.api.libs.json.{Json, Writes}

sealed trait ErrorMessage {
  val message: String
}

object ErrorMessage {
  type ErrorOr[T] = Either[ErrorMessage, T]

  implicit val writes: Writes[ErrorMessage] = Writes[ErrorMessage]((msg: ErrorMessage) => Json.obj("message" -> msg.message))
}

case object AuthenticationFailure extends ErrorMessage {
  val message = "Authentication failed"
}

case class ValidationError(message: String) extends ErrorMessage {
  def merge(other: ValidationError): ValidationError = ValidationError(s"$message, ${other.message}")
}

case object GenericError extends ErrorMessage {
  val message = "An error occurred"
}