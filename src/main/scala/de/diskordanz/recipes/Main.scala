package de.diskordanz.recipes

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import de.diskordanz.recipes.auth.UserRepository
import de.diskordanz.recipes.config.Configuration
import de.diskordanz.recipes.db.Database
import de.diskordanz.recipes.recipe.RecipeRepository

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Main extends LazyLogging {

  private val config = Configuration.fromEnvironment()

  private def startHttpServer(route: Route)(implicit system: ActorSystem, executionContext: ExecutionContext): Unit = {

    val futureBinding = Http().newServerAt(config.interface, config.port).bind(route)
    futureBinding.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        logger.info(s"Server online at http://${address.getHostString}:${address.getPort}/")
      case Failure(ex) =>
        logger.error("Failed to bind HTTP endpoint, terminating system", ex)
        system.terminate()
    }
  }

  private def getDbConnection = Database.getConnection(s"${config.dbUrl}/${Database.Name}")

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem("server")
    implicit val materializer: Materializer = Materializer(system)
    implicit val executionContext: ExecutionContext = system.dispatcher

    for {
      db <- getDbConnection
      userRepo <- UserRepository.initialize(db, config)
      recipeRepo <- RecipeRepository.initialize(db)
    } yield {
      val routes = new Routes(recipeRepo, userRepo, config).routes
      startHttpServer(routes)
    }
  }
}
