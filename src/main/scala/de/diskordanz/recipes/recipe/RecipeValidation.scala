package de.diskordanz.recipes.recipe

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.Directives._
import de.diskordanz.recipes.api.{ErrorMessage, ValidationError}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport


trait RecipeValidation extends PlayJsonSupport {

  val entityAsGenerateRecipeCommand: Directive1[GenerateRecipeCommand] =
    entity(as[GenerateRecipeCommand]).flatMap { command =>
      val errors = Seq(
        notBlank("name", command.name),
        notBlank("preparation", command.preparation),
        atLeastOne("portions", command.servings)
      ).flatten

      if (errors.nonEmpty) {
        val message: ErrorMessage = errors.reduce(_ merge _)
        complete(StatusCodes.BadRequest, message)
      } else {
        provide(command)
      }
    }

  val entityAsRecipe: Directive1[Recipe] =
    entity(as[Recipe]).flatMap { recipe =>
      val errors = Seq(
        notBlank("name", recipe.name),
        notBlank("preparation", recipe.preparation),
        atLeastOne("portions", recipe.servings)
      ).flatten

      if (errors.nonEmpty) {
        val message: ErrorMessage = errors.reduce(_ merge _)
        complete(StatusCodes.BadRequest, message)
      } else {
        provide(recipe)
      }
    }

  private def notBlank(field: String, value: String): Option[ValidationError] = {
    if (value.isBlank) {
      Some(ValidationError(s"$field must not be blank"))
    } else {
      None
    }
  }

  private def atLeastOne(field: String, value: Long): Option[ValidationError] = {
    if (value < 1) {
      Some(ValidationError(s"$field must at least be 1"))
    } else {
      None
    }
  }
}
