package de.diskordanz.recipes.recipe


import de.diskordanz.recipes.Slugifiable._
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import play.api.libs.json.{Json, Reads, Writes}


case class GenerateRecipeCommand(name: String,
                                 preparation: String,
                                 tags: Seq[String],
                                 ingredients: Seq[String],
                                 duration: String,
                                 servings: Int) {

  def toRecipe: Recipe = {
    val slug = name.slugified
    Recipe(name, slug, preparation, tags, ingredients, duration, servings)
  }
}

object GenerateRecipeCommand extends PlayJsonSupport {
  implicit val jsonr: Reads[GenerateRecipeCommand] = Json.reads[GenerateRecipeCommand]
  implicit val jsonw: Writes[GenerateRecipeCommand] = Json.writes[GenerateRecipeCommand]
}