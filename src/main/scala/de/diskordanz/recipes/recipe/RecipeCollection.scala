package de.diskordanz.recipes.recipe

import com.typesafe.scalalogging.LazyLogging
import de.diskordanz.recipes.api.ErrorMessage.ErrorOr
import de.diskordanz.recipes.api.GenericError
import de.diskordanz.recipes.db.{AbstractCollection, MongoIndex}
import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.indexes.Index.Aux
import reactivemongo.api.indexes.IndexType.Ascending
import reactivemongo.api.{Cursor, DB}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

trait RecipeRepository {
  def search(query: String): Future[Seq[LightRecipe]]

  def findAll: Future[Seq[Recipe]]

  def findBySlug(slug: String): Future[Recipe]

  def insert(generateRecipeCommand: GenerateRecipeCommand): Future[ErrorOr[LightRecipe]]

  def update(slug: String, recipe: Recipe): Future[ErrorOr[LightRecipe]]

  def delete(slug: String): Future[ErrorOr[Unit]]
}

object RecipeRepository extends LazyLogging {
  def initialize(db: DB): Future[RecipeRepository] = {
    val collection = new RecipeCollection(db)
    for {
      _ <- collection.ensureIndexes()
    } yield {
      logger.info("Initialized recipe repository")
      collection
    }
  }.recoverWith {
    case e =>
      logger.error("Failed initializing recipe repository", e)
      Future.failed(e)
  }
}

private class RecipeCollection(db: DB) extends AbstractCollection(RecipeCollection.Name, db) with RecipeRepository {

  private val projectToRecipe: Option[BSONDocument] = None

  private val sortBySlug: BSONDocument = BSONDocument("slug" -> 1)

  private def bySlug(slug: String) = BSONDocument("slug" -> slug)

  override def search(query: String): Future[Seq[LightRecipe]] = {
    collection.find(BSONDocument("$text" -> BSONDocument("$search" -> query)), projectToRecipe)
      .cursor[LightRecipe]()
      .collect[List](-1, Cursor.FailOnError())
  }

  def findAll: Future[Seq[Recipe]] = {
    collection.find(BSONDocument.empty, projectToRecipe)
      .sort(sortBySlug)
      .cursor[Recipe]()
      .collect[List](-1, Cursor.FailOnError())
  }

  def findBySlug(slug: String): Future[Recipe] = {
    collection.find(bySlug(slug), projectToRecipe)
      .requireOne[Recipe]
  }

  def insert(generateRecipeCommand: GenerateRecipeCommand): Future[ErrorOr[LightRecipe]] = {
    val recipe = generateRecipeCommand.toRecipe.withFilteredIngredients
    val insertResult = collection.insert(ordered = false)
      .one(recipe)
      .transform {
        case Success(writeResult) => Success(Right())
        case _ => Success(Left(GenericError))
      }
    insertResult.flatMap { _ =>
      findBySlug(recipe.slug).map(r => Right(r.toLight))
    }
  }

  def update(slug: String, recipe: Recipe): Future[ErrorOr[LightRecipe]] = {
    val updateResult = collection.update(ordered = false)
      .one(bySlug(slug), recipe.withFilteredIngredients, upsert = false, multi = false)
      .transform {
        case Success(writeResult) => Success(Right())
        case _ => Success(Left(GenericError))
      }
    updateResult.flatMap { _ =>
      findBySlug(recipe.slug).map(r => Right(r.toLight))
    }
  }

  def delete(slug: String): Future[ErrorOr[Unit]] = {
    collection.delete(ordered = false)
      .one(bySlug(slug))
      .transform {
        case Success(writeResult) => Success(Right())
        case _ => Success(Left(GenericError))
      }
  }

  override protected val indexes: Seq[Aux[BSONSerializationPack.type]] = Seq(
    MongoIndex.unique("slug" -> Ascending),
    MongoIndex.fullTextOnFields("german", fields = Seq("name"))
  )

}

private object RecipeCollection {
  val Name = "recipes"
}