package de.diskordanz.recipes.recipe

import de.diskordanz.recipes.Slugifiable._
import play.api.libs.json._
import reactivemongo.api.bson.{BSONDocumentReader, BSONDocumentWriter, Macros}

import scala.util.Try

case class Recipe(name: String,
                  slug: String,
                  preparation: String,
                  tags: Seq[String],
                  ingredients: Seq[String],
                  duration: String,
                  servings: Int,
                  image: Option[String] = None) {

  def withGeneratedSlug: Recipe = copy(slug = name.slugified)

  def withFilteredIngredients: Recipe = {
    val filteredIngredients = ingredients.filter(_.nonEmpty)
    copy(ingredients = filteredIngredients)
  }

  def toLight: LightRecipe = LightRecipe(name, slug)

}

object Recipe {

  implicit val jsonr: Reads[Recipe] = Json.reads[Recipe]
  implicit val jsonw: Writes[Recipe] = Json.writes[Recipe]

  implicit val bsonw: BSONDocumentWriter[Recipe] = Macros.writer[Recipe]
  implicit val bsonr: BSONDocumentReader[Recipe] = Macros.reader[Recipe]

}

case class LightRecipe(name: String, slug: String)

object LightRecipe {
  implicit val jsonr: Reads[LightRecipe] = Json.reads[LightRecipe]
  implicit val jsonw: Writes[LightRecipe] = Json.writes[LightRecipe]
  implicit val bsonr: BSONDocumentReader[LightRecipe] = Macros.reader[LightRecipe]
}

case class ImportRecipe(
                       name: String,
                       preparation: String,
                       preparation_time: Option[String],
                       servings: Int,
                       tags: Option[Seq[String]],
                       ingredients: Seq[ImportIngredient]
                       ) {
  def toCommand: GenerateRecipeCommand = GenerateRecipeCommand(
    name = name,
    preparation = preparation,
    tags = tags.getOrElse(Nil),
    ingredients = ingredients.map(_.toIngredient),
    duration = preparation_time.getOrElse(""),
    servings = servings
  )
}

object ImportRecipe {
  implicit val jsonr: Reads[ImportRecipe] = Json.reads[ImportRecipe]
}

case class ImportIngredient(
                           ingredient_name: String,
                           amount: Option[JsValue]
                           ) {

  def toIngredient: String = {
    val amountAsString = amount.flatMap { a =>
      Try(a.as[Int] + " ")
        .orElse(Try(a.as[String] + " "))
        .toOption
    }.getOrElse("")
    s"$amountAsString$ingredient_name"

  }
}

object ImportIngredient {
  implicit val jsonr: Reads[ImportIngredient] = Json.reads[ImportIngredient]
}
