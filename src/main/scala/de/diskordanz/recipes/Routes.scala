package de.diskordanz.recipes

import java.io.File
import java.nio.file.Files

import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.HttpOrigin
import akka.http.scaladsl.model.{MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, RejectionHandler, Route}
import akka.stream.scaladsl.FileIO
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import ch.megard.akka.http.cors.scaladsl.model.HttpOriginMatcher
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.typesafe.scalalogging.LazyLogging
import de.diskordanz.recipes.api.{ErrorMessage, ValidationError}
import de.diskordanz.recipes.auth.User.Roles
import de.diskordanz.recipes.auth.{LoginCredentials, SessionHandling, UserCreationCommand, UserRepository}
import de.diskordanz.recipes.config.Configuration
import de.diskordanz.recipes.recipe.{ImportRecipe, RecipeRepository, RecipeValidation}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}
import scala.util.{Failure, Random, Success}


class Routes(recipeCollection: RecipeRepository, val userRepository: UserRepository, val config: Configuration)
            (implicit val system: ActorSystem)
  extends SessionHandling with RecipeValidation with PlayJsonSupport with LazyLogging {

  private val recipeRoutes: Route =
    pathPrefix("recipes") {
      authenticated { _ =>
        concat(
          concat(
            get {
              concat(
                pathEndOrSingleSlash {
                  complete(StatusCodes.OK, recipeCollection.findAll)
                },
                pathPrefix("search") {
                  parameter('q.as[String]) { query =>
                    onSuccess(recipeCollection.search(query)) { results =>
                      complete(StatusCodes.OK, results)
                    }
                  }
                })
            },
            post {
              concat(
                pathEndOrSingleSlash {
                  entityAsGenerateRecipeCommand { generateRecipeCommand =>
                    onSuccess(recipeCollection.insert(generateRecipeCommand)) {
                      case Right(inserted) => complete(StatusCodes.OK, inserted)
                      case _ => complete(StatusCodes.InternalServerError)
                    }
                  }
                },
                pathPrefix("import") {
                  entity(as[Seq[ImportRecipe]]) { importFile =>
                    val importResults = for {
                      entry <- importFile
                    } yield {
                      recipeCollection.insert(entry.toCommand)
                    }
                    onSuccess(Future.sequence(importResults)) { _ =>
                      complete(StatusCodes.OK)
                    }
                  }
                }
              )
            },
            put {
              concat(
                pathPrefix(Segment) { slug =>
                  entityAsRecipe { recipe =>
                    if (slug != recipe.slug) {
                      val error: ErrorMessage = ValidationError("Slug from path did not match slug from payload")
                      complete(StatusCodes.BadRequest, error)
                    } else {
                      onSuccess(recipeCollection.update(slug, recipe.withGeneratedSlug)) {
                        case Right(updated) => complete(StatusCodes.OK, updated)
                        case _ => complete(StatusCodes.InternalServerError)
                      }
                    }
                  }
                },
                pathPrefix(Segment / "image") { slug =>
                  onSuccess(recipeCollection.findBySlug(slug)) { recipe =>
                    extractRequestEntity { entity =>
                      val contentType = entity.contentType
                      if (contentType != MediaTypes.`image/jpeg`.toContentType) {
                        complete(StatusCodes.BadRequest)
                      } else {
                        val tempFile = blocking {
                          File.createTempFile(s"image-$slug", "").toPath
                        }
                        val finalFileName = s"${Random.alphanumeric.take(30).mkString}.jpg"
                        val finalFilePath = config.imageDirectory.resolve(finalFileName)
                        val writeResult = entity.dataBytes.runWith(FileIO.toPath(tempFile)).map { _ =>
                          blocking {
                            Files.copy(tempFile, finalFilePath)
                          }
                        }
                        onComplete(writeResult) {
                          case Success(_) => {
                            val updateResult = recipeCollection.update(recipe.slug, recipe.copy(image = Some(finalFileName)))
                            onSuccess(updateResult) { _ =>
                              complete(s"/images/${finalFileName}")
                            }
                          }
                          case _ => complete(StatusCodes.InternalServerError)
                        }
                      }
                    }
                  }
                }
              )
            }
          ),
          pathPrefix(Segment) { slug =>
            concat(
              pathEndOrSingleSlash {
                concat(
                  get {
                    onComplete(recipeCollection.findBySlug(slug)) {
                      case Success(recipe) => complete(StatusCodes.OK, recipe)
                      case Failure(cause) =>
                        logger.warn("Recipe not found: {}", slug, cause)
                        complete(StatusCodes.NotFound)
                    }
                  },
                  delete {
                    onComplete(recipeCollection.delete(slug)) {
                      case Success(_) =>
                        logger.info("Deleted recipe: {}", slug)
                        complete(StatusCodes.OK)
                      case Failure(cause) =>
                        logger.warn("Failed deleting recipe: {}", slug, cause)
                        complete(StatusCodes.InternalServerError)
                    }
                  })
              }
            )
          }
          ,
        )
      }
    }

  private val userRoutes: Route =
    pathPrefix("auth") {
      concat(
        path("login") {
          post {
            entity(as[LoginCredentials]) {
              credentials =>
                tryLoggingInUser(credentials)
            }
          }
        },
        path("logout") {
          authenticated { user =>
            post {
              logoutUser(user)
            }
          }
        },
        path("session") {
          authenticated { user =>
            complete(StatusCodes.OK, user.toSessionInfo)
          }
        },
        pathPrefix("users") {
          authenticated { user =>
            post {
              entity(as[UserCreationCommand]) { command =>
                if (user.role != Roles.Admin) {
                  complete(StatusCodes.Forbidden)
                } else {
                  onSuccess(userRepository.insert(command.toUser)) {
                    logger.info(s"Created user ${command.user}")
                    complete(StatusCodes.OK)
                  }
                }
              }
            }
          }
        }
      )
    }

  val staticRoutes: Route = concat(
    pathEndOrSingleSlash {
      getFromResource("static/index.html")
    },
    path(RemainingPath) { filePath =>
      getFromResource(s"static/$filePath")
    })

  private val corsSettings = CorsSettings.defaultSettings.withAllowedOrigins(HttpOriginMatcher(HttpOrigin(config.frontendCORSOrigin)))

  val rejectionHandler = corsRejectionHandler.withFallback(RejectionHandler.default)

  // Your exception handler
  val exceptionHandler = ExceptionHandler {
    case e: NoSuchElementException => complete(StatusCodes.NotFound)
  }

  // Combining the two handlers only for convenience
  val handleErrors = handleRejections(rejectionHandler) & handleExceptions(exceptionHandler)

  val routes: Route = cors() {
    handleErrors {
      pathPrefix("api") {
        userRoutes ~ recipeRoutes
      } ~ staticRoutes
    }
  }

}
