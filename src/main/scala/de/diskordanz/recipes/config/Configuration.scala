package de.diskordanz.recipes.config

import java.nio.file.{Files, Path, Paths}

import scala.util.Properties.{envOrElse, envOrNone}

trait Configuration {

  def frontendCORSOrigin: String
  def dbUrl: String
  def port: Int
  def interface: String
  def adminPassword: Option[String]
  def imageDirectory: Path
}

object Configuration {

  def fromEnvironment(): Configuration = new EnvironmentConfiguration()

  private[this] class EnvironmentConfiguration extends Configuration {

    val frontendCORSOrigin: String = envOrElse("FRONTEND_ORIGIN", "http://localhost:8081")

    val dbUrl: String = envOrElse("DB_URL", "mongodb://localhost:27017")

    val port: Int = envOrElse("PORT", "8080").toInt
    val interface: String = envOrElse("INTERFACE", "localhost")

    val adminPassword: Option[String] = envOrNone("ADMIN_PASSWORD")

    val imageDirectory: Path = {
      val rawPath = Paths.get(envOrElse("IMAGE_DIR", "/tmp/"))
      if (Files.isDirectory(rawPath)) {
        rawPath.toAbsolutePath
      } else {
        throw new IllegalArgumentException("IMAGE_DIR must contain a valid directory path")
      }
    }
  }
}
