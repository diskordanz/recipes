package de.diskordanz.recipes

import com.github.slugify.Slugify

object Slugifiable {

  implicit class SlugifiableString(string: String) {
    def slugified: String = new Slugify().slugify(string)
  }
}
