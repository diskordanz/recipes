package de.diskordanz.recipes.util

import java.nio.file.{Files, Path}

import de.diskordanz.recipes.config.Configuration

class TestConfiguration extends Configuration {

  var frontendCORSOrigin: String = "http://localhost"

  var dbUrl: String = ""

  var port: Int = 0

  var interface: String = ""

  var adminPassword: Option[String] = None

  var imageDirectory: Path = Files.createTempDirectory("test")
}
