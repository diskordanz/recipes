package de.diskordanz.recipes.util

import com.dimafeng.testcontainers.{ForAllTestContainer, MongoDBContainer}
import org.scalatest.flatspec.AnyFlatSpec


trait TestMongoDB extends AnyFlatSpec with ForAllTestContainer {

  override val container: MongoDBContainer = MongoDBContainer("mongo:4.2.10")

}