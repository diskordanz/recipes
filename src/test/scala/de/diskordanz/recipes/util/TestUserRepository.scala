package de.diskordanz.recipes.util

import de.diskordanz.recipes.auth.{LoginCredentials, User, UserRepository}

import scala.concurrent.Future

object TestUserRepository extends UserRepository {

  val testAdminCredentials: LoginCredentials = LoginCredentials(userName = "test", password = "test")
  val testAdmin: User = User.withPassword(
    name = testAdminCredentials.userName, password = testAdminCredentials.password, User.Roles.Admin)

  override def findByName(name: String): Future[Option[User]] = if (name == testAdmin.name) {
    Future.successful(Some(testAdmin))
  } else {
    Future.successful(None)
  }

  override def update(user: User): Future[Unit] = Future.successful()

  override def insert(user: User): Future[Unit] = Future.successful()
}
