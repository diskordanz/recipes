package de.diskordanz.recipes


import java.io.File

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.scaladsl.FileIO
import de.diskordanz.recipes.db.Database
import de.diskordanz.recipes.recipe.{GenerateRecipeCommand, LightRecipe, Recipe, RecipeRepository}
import de.diskordanz.recipes.util.{TestConfiguration, TestMongoDB, TestUserRepository}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.{IntegrationPatience, ScalaFutures}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import play.api.libs.json.Json

import scala.concurrent.Future
import scala.util.Try

class RoutesTest extends AnyFlatSpec with Matchers with ScalatestRouteTest with MockFactory with PlayJsonSupport
  with BeforeAndAfterAll with TestMongoDB with ScalaFutures with IntegrationPatience with BeforeAndAfterEach
  with TableDrivenPropertyChecks {

  private val userRepo = TestUserRepository
  private val config = new TestConfiguration()

  private lazy val routes = new Routes(recipeRepo, userRepo, config)

  private val loginEntity = userRepo.testAdminCredentials.toJson.toString()
  private val recipeCommand = GenerateRecipeCommand("name", "preparation", Seq("tag"), Seq("ingredient"), "duration", servings = 23)
  private val recipe = recipeCommand.toRecipe

  var authHeader: HttpHeader = null

  private lazy val dbConn = Database.getConnection(s"${config.dbUrl}/${Database.Name}").futureValue
  private lazy val recipeRepo = RecipeRepository.initialize(dbConn).futureValue

  override def beforeAll(): Unit = {
    super.beforeAll()
    config.dbUrl = container.replicaSetUrl
    authHeader = login()
  }

  override def beforeEach(): Unit = {
    deleteAllRecipes()
    config.imageDirectory.toFile.listFiles().map(f => f.delete())
  }

  "The API" should "allow login" in {
    Get("/api/auth/session") ~> routes.routes ~> check {
      status shouldBe StatusCodes.Unauthorized
    }

    Get("/api/auth/session").withHeaders(authHeader) ~> routes.routes ~> check {
      status shouldBe StatusCodes.OK
    }
  }

  it should "create, list and delete recipes" in {
    createRecipe(recipeCommand) shouldBe Some(recipe.toLight)

    getAllRecipes shouldBe Seq(recipe.toLight)

    getRecipe(recipe.slug) shouldBe Some(recipe)

    getRecipe("unknown", expectedStatusCode = StatusCodes.NotFound) shouldBe None

    deleteRecipe(recipe.slug)

    getAllRecipes should have length 0

    getRecipe(recipe.slug, expectedStatusCode = StatusCodes.NotFound) shouldBe None
  }

  private val invalidGenerateCommands = Table(
    heading = "command",
    rows =
      recipeCommand.copy(name = ""),
      recipeCommand.copy(name = " "),
      recipeCommand.copy(preparation = ""),
      recipeCommand.copy(preparation = " "),
      recipeCommand.copy(servings = -1),
      recipeCommand.copy(servings = 0),
  )

  it should "reject creation of invalid recipes" in {
    forAll(invalidGenerateCommands) { command =>
      createRecipe(command, expectedStatusCode = StatusCodes.BadRequest) shouldBe None
    }
  }

  it should "allow updating a recipe" in {
    createRecipe(recipeCommand) shouldBe Some(recipe.toLight)
    getRecipe(recipe.slug) shouldBe Some(recipe)

    val updated = recipe.copy(
      name = "newName",
      preparation = "newPreparation",
      tags = Seq("newTag"),
      ingredients = Seq("newIngredient"),
      duration = "newDuration",
      servings = recipe.servings + 1)

    updateRecipe(recipe.slug, updated) shouldBe Some(updated.withGeneratedSlug.toLight)
    getAllRecipes shouldBe Seq(updated.withGeneratedSlug.toLight)
    getRecipe(recipe.slug, expectedStatusCode = StatusCodes.NotFound) shouldBe None
    getRecipe(updated.withGeneratedSlug.slug) shouldBe Some(updated.withGeneratedSlug)
  }

  private val invalidRecipes = Table(
    heading = "recipe",
    rows =
      recipe.copy(name = ""),
      recipe.copy(name = " "),
      recipe.copy(preparation = ""),
      recipe.copy(preparation = " "),
      recipe.copy(servings = -1),
      recipe.copy(servings = 0),
  )

  it should "reject invalid recipe updates" in {
    createRecipe(recipeCommand) shouldBe Some(recipe.toLight)
    getRecipe(recipe.slug) shouldBe Some(recipe)

    forAll(invalidRecipes) { invalidRecipe =>
      updateRecipe(recipe.slug, invalidRecipe, expectedStatusCode = StatusCodes.BadRequest) shouldBe None
      getAllRecipes shouldBe Seq(recipe.toLight)
    }
  }

  it should "reject recipe updates with mismatched slug" in {
    createRecipe(recipeCommand) shouldBe Some(recipe.toLight)
    getRecipe(recipe.slug) shouldBe Some(recipe)

    val updated = recipe.copy(
      name = "newName",
      preparation = "newPreparation",
      tags = Seq("newTag"),
      ingredients = Seq("newIngredient"),
      duration = "newDuration",
      servings = recipe.servings + 1)

    // this will result in a mismatch between request slug and payload slug, as the new slug is different
    updateRecipe(recipe.slug, updated.withGeneratedSlug, expectedStatusCode = StatusCodes.BadRequest) shouldBe None
    getAllRecipes shouldBe Seq(recipe.toLight)
  }

  it should "allow image upload" in {
    createRecipe(recipeCommand)
    val image = "thisisanimage".getBytes
    uploadedImages should have length 0

    val uploadedImageName = uploadImage(recipe.slug, image)

    uploadedImageName shouldBe defined
    uploadedImages.map(_.getName) shouldBe Seq(uploadedImageName.get)
    uploadedImageContent(uploadedImages.head) shouldBe image
  }

  it should "not allow uploading an image for a nonexistent recipe" in {
    val image = "thisisanotherimage".getBytes

    uploadedImages should have length 0

    uploadImage("unknown", image, expectedStatusCode = StatusCodes.NotFound) shouldBe None
    uploadedImages should have length 0
  }

  private def deleteAllRecipes() = {
    val allRecipes = recipeRepo.findAll.futureValue
    Future.sequence(allRecipes.map(r => recipeRepo.delete(r.slug))).futureValue
    recipeRepo.findAll.futureValue should have length 0
  }

  private def createRecipe(command: GenerateRecipeCommand, expectedStatusCode: StatusCode = StatusCodes.OK): Option[LightRecipe] = {
    var created: Option[LightRecipe] = None
    Post("/api/recipes/")
      .withHeaders(authHeader)
      .withEntity(ContentTypes.`application/json`, Json.toJson(command).toString()) ~> routes.routes ~> check {
      status shouldBe expectedStatusCode
      created = Try(entityAs[LightRecipe]).toOption
    }
    created
  }

  private def updateRecipe(slug: String, toUpdate: Recipe, expectedStatusCode: StatusCode = StatusCodes.OK): Option[LightRecipe] = {
    var updated: Option[LightRecipe] = None
    Put(s"/api/recipes/$slug")
      .withHeaders(authHeader)
      .withEntity(ContentTypes.`application/json`, Json.toJson(toUpdate).toString()) ~> routes.routes ~> check {
      status shouldBe expectedStatusCode
      updated = Try(entityAs[LightRecipe]).toOption
    }
    updated
  }

  private def getAllRecipes: Seq[LightRecipe] = {
    var allRecipes: Seq[LightRecipe] = Nil
    Get("/api/recipes/").withHeaders(authHeader) ~> routes.routes ~> check {
      status shouldBe StatusCodes.OK
      allRecipes = entityAs[Seq[LightRecipe]]
    }
    allRecipes
  }

  private def getRecipe(slug: String, expectedStatusCode: StatusCode = StatusCodes.OK): Option[Recipe] = {
    var recipe: Option[Recipe] = None
    Get(s"/api/recipes/$slug").withHeaders(authHeader) ~> routes.routes ~> check {
      status shouldBe expectedStatusCode
      recipe = Try(entityAs[Recipe]).toOption
    }
    recipe
  }

  private def deleteRecipe(slug: String) = {
    Delete(s"/api/recipes/$slug").withHeaders(authHeader) ~> routes.routes ~> check {
      status shouldBe StatusCodes.OK
    }
  }

  private def uploadImage(slug: String, imageBytes: Array[Byte], expectedStatusCode: StatusCode = StatusCodes.OK): Option[String] = {
    var uploadedImageName: Option[String] = None
    val imageEntity = HttpEntity(MediaTypes.`image/jpeg`, imageBytes)

    Put(s"/api/recipes/$slug/image").withHeaders(authHeader).withEntity(imageEntity) ~> routes.routes ~> check {
      status shouldBe expectedStatusCode
      if (status == StatusCodes.OK) {
        val response = entityAs[String]
        response should startWith("/images/")
        response should endWith(".jpg")
        uploadedImageName = Some(response.stripPrefix("/images/"))
      }
    }
    uploadedImageName
  }

  private def uploadedImages: Seq[File] = config.imageDirectory.toFile.listFiles

  private def uploadedImageContent(image: File): Array[Byte] = FileIO.fromPath(image.toPath).runReduce(_ ++ _).futureValue.toByteBuffer.array()

  private def login(): HttpHeader = {
    var token = ""

    Post("/api/auth/login").withEntity(ContentTypes.`application/json`, loginEntity) ~> routes.routes ~> check {
      status shouldBe StatusCodes.OK
      val setAuthHeader = header("Set-Authorization")
      setAuthHeader should not be empty
      token = setAuthHeader.get.value
      token should not be empty
    }

    RawHeader("Authorization", s"Bearer $token")
  }

  override def afterAll(): Unit = {
    super.afterAll()
  }
}
