FROM openjdk:11-jre-slim

RUN useradd java
COPY --chown=java:java target/scala-2.12/recipes.jar .
USER java

ENTRYPOINT ["java"]
CMD ["-jar", "recipes.jar"]
